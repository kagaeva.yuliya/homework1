N = int(input('количество учеников в классе: '))
tr = 0
hor = 0
otl = 0
for i in range(1, N+1):
    score = int(input('введите оценку от 3 до 5: '))
    if score == 3:
        tr += 1
    elif score == 4:
        hor += 1
    elif score == 5:
        otl += 1
if otl < tr > hor:
    print('сегодня больше троечников')
elif otl < hor > tr:
    print('сегодня больше хорошистов')
else:
    print('сегодня больше отличников')