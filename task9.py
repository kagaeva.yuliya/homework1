my_number = int(input('загаданное число: '))
count = 0
while True:
  number = int(input('введите число '))
  if number < my_number:
    print('Число меньше, чем нужно. Попробуйте ещё раз!')
  elif number > my_number:
    print('Число больше, чем нужно. Попробуйте ещё раз!')
  count += 1
  if number == my_number:
    print('Вы угадали! Число попыток: ', count)
    break